// manager.js: Entry point for COMPSCIDiscordBot project.
// Listens out for commands with a specific prefix,
// loads the relevant command module and then 
// attempts to execute that command.

const fs = require("fs");
const path = require("path");
const Discord = require("discord.js");
const utils = require("./src/util.js");
const config = require("./config/config.json");
const auth = require("./config/auth.json");
const client = new Discord.Client();

let botCommandsChannel;
let developmentChannel;

client.on("ready", () => {
    botCommandsChannel = client.guilds.find("name", "UoA COMPSCI").channels.find("name", "bot-commands");
    developmentChannel = client.guilds.find("name", "UoA COMPSCI").channels.find("name", "development");

    console.log("Class Manager started.");
    client.user.setActivity("for @Class Manager help", { type: "WATCHING"});
    developmentChannel.send("<@459211651149987852> started.");
});

// Runs with every seen message.
client.on("message", async message => {
    if (message.author.bot) return;
    if (message.guild === null || message.guild.name !== "UoA COMPSCI") {
        message.reply("Commands can only be used inside the `UoA COMPSCI` server.");
        return;
    }
    if (message.channel != botCommandsChannel && message.channel != developmentChannel) return;

    let mentionRegEx = new RegExp("^<@!?~?\\d+>(\\s*\\+)?", "g");
    if (message.isMentioned(client.user) && message.content.match(mentionRegEx)) { // If @Class Manager <command>
        var args = message.content.replace(mentionRegEx, "");
    }
    else if (message.content.indexOf(config.prefix) == 0) { // If (prefix)command
        var args = message.content.slice(config.prefix.length);
    }
    else return;

    args = args.trim().split(/[\n\r\s]+/g);
    let command = args.shift().toLowerCase();

    if (!command) {
        message.channel.send("Need something, " + message.member + "? Try `help`!");
        return;
    }

    // Load and run command from file.
    message.channel.startTyping();
    commandPath = "./src/commands/" + path.basename(command) + ".js";
    if (!fs.existsSync(commandPath)) {
        message.react("⚠");
        message.reply("the command '" + command + "' doesn't exist. Try `help`.");
        message.channel.stopTyping(true);
        return;
    }
    try {
        let commandFile = require(commandPath);
        let userRoleNames = message.member.roles.map(role => role.name);

        // If user does not have a role that is defined as allowed in command file:
        if (!userRoleNames.some(role => commandFile.accessLevels.includes(role))) {
            message.react("🚫");
            message.reply("you do not have access to this command.");
            message.channel.stopTyping(true);
            return;
        }
        
        await commandFile.run(client, message, args, config);
        message.react("✅");
        message.channel.stopTyping(true);
    }
    catch (err) {
        console.error(err);
        message.react("⚠");
        message.reply("unable to run the command `" +  command + "`.");
        message.channel.stopTyping(true);
    }
});

// When a new user joins, check which invites were incremented and assign roles accordingly.
client.on("guildMemberAdd", async member => {
    if (member.user.bot) return;
    utils.assignRoles(member);
});

client.login(auth.token);

process
.on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
    developmentChannel.send("Unhandled Rejection at Promise: ```" + reason.substring(0, 1880) + "```"); // max msg size 2k chars
  })
  .on('uncaughtException', err => {
    console.error(err, 'Uncaught Exception thrown');
    developmentChannel.send("Uncaught Exception thrown: ```" + err.message + "```\n```" + err.stack + "```\n```" + JSON.stringify(err) + "```");
  });

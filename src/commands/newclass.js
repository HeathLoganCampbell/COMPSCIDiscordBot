// newclass.js: Contains module for command 'newclass'.
// Allows an administrator to create the given class(es).
// Sets up a new role, channel, and creates an invite for each class.

const iData = require("../invites.js");

exports.accessLevels = ["Administrator", "Badministrator"];

exports.help = "`newclass <class> [class2] [classN]` - sets up a new channel, role, and creates an invite.";

exports.run = async (client, message, args, config) => {
    if (args.length < 1) {
        message.channel.send("Usage: " + exports.help);
        return;
    }

    args = [...(new Set(args))]; // Remove duplicates.
    const server = message.guild;
    const defaultRole = server.roles.find("name", "@everyone");
    const modRole = server.roles.find("name", "Moderator");
    const botRole = server.roles.find("name", "Class Manager");

    for (i = 0; i < args.length; i++) {
        let className = args[i];
        let stageNumber = className.slice(-3, -2);
        let role = server.roles.find("name", className);
        let category = server.channels.find("name", "Stage " + stageNumber);
        let channel = server.channels.find("name", className);

        if (!role) role = await server.createRole({ name: className });
        await role.edit({
            //mentionable: true,    // If enabled, allows every user to mention the role. Not good! Class Reps+ can use @everyone (in respective channel) instead.
            permissions: 103926849  // https://i.imgur.com/dCp0YhX.png
        });

        if (!channel) channel = await server.createChannel(className);
        channel.setParent(category);
        channel.overwritePermissions(defaultRole, {"VIEW_CHANNEL": false });
        channel.overwritePermissions(role, {"VIEW_CHANNEL": true });
        channel.overwritePermissions(modRole, {"VIEW_CHANNEL": true });
        await channel.overwritePermissions(botRole, {"VIEW_CHANNEL": true });

        let invite = await server.fetchInvites();
        invite = invite.find("channel", channel);
        if (!invite) {
            invite = await channel.createInvite({ maxAge: 0, unique: true });
        }
        if (!iData.getInvites()[invite.code]) {
            iData.addRoleFor(invite.code, role.id);
        }

        let pinnedMessages = await channel.fetchPinnedMessages();
        pinnedMessages = pinnedMessages.filterArray(message => message.author == client.user);
        if (pinnedMessages.length === 0) {
            channel.send("Invite link to this class: " + invite.url).then(sentMessage => sentMessage.pin());
            continue;
        }

        pinnedMessages.forEach(message => {
            if (message.content.startsWith("Invite link to this class: ")) {
                message.edit("Invite link to this class: " + invite.url)
            }
        });
    }

    let channelPositions = [];
    let channels = message.guild.channels.array().filter(channel => channel.name.startsWith(config.domain));
    channels.sort((a, b) => a.name.localeCompare(b.name));

    channels.forEach((channel, index) => {
        channelPositions.push({ channel: channel, position: index });
    });
    server.setChannelPositions(channelPositions);

    message.reply("the roles, channels and invites for each specified class have been created!\n" +
        "The invite links have been posted and pinned to each class channel.");
};

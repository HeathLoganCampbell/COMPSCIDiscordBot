// show.js: Contains module for command 'show'.
// Displays a list of classes the user is subscribed to.

exports.accessLevels = ["@everyone"];

exports.help = "`show` - shows classes you're subscribed to.";

exports.run = (client, message, args, config) => {
    let roles = message.member.roles.array();
    roles = roles.filter(role => role.name.startsWith(config.domain + "-"));

    if (roles.length == 0) {
        message.reply("you're not currently subscribed to any classes.");
        return;
    }
    
    let responseMessage = "you're currently subscribed to " + roles.length + " " +
        (roles.length > 1 ?  "classes:" : "class:") + "\n";

    roles.forEach(role => {
        let channel = message.guild.channels.find("name", role.name);
        responseMessage += (channel ? channel.toString() : ("`" + role.name + "`")) + "\n"; // If the role exists but not the channel (shouldn't happen)
    });

    message.reply(responseMessage);
}

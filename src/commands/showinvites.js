// showinvites.js: Contains module for command 'showinvites'.
// Allows an administrator to display the created invite links
// with class subscriptions.

const iData = require("../invites.js");

exports.accessLevels = ["Administrator", "Badministrator"];

exports.help = "`showinvites` - shows all invites with class subscriptions on join.";

exports.run = async (client, message, args, config) => {
    let storedInvites = iData.getInvites();
    let guildInvites = await message.member.guild.fetchInvites();
    let response = "the format for links is <https://discord.gg/>`<code>`\n";

    for (var inviteCode in storedInvites) {
        if (!storedInvites.hasOwnProperty(inviteCode)) continue;

        if (!guildInvites.get(inviteCode)) {
            iData.remove(inviteCode);
            continue;
        }

        let inv = storedInvites[inviteCode];
        response += "\nInvite code: `" + inviteCode
            + "`. Usage count: `"+ inv["usages"] + "`. Classes:";
        for (var roleIndex = 0; roleIndex < inv["roles"].length; roleIndex++) {
            var roleID = inv["roles"][roleIndex];
            var role = await message.guild.roles.get(roleID);

            if (!role) {
                iData.removeRoleFrom(inviteCode, roleID); // The invite is no longer valid.
                roleIndex--;
                continue;
            }

            response += " " + await message.guild.channels.find("name", role.name);
        }
    }

    if (Object.keys(storedInvites).length < 1) {
        message.reply("there are currently no invites.");
        return;
    }

    message.reply(response);
}

// list.js: Contains module for command 'list'.
// Displays a list of the classes available.

exports.accessLevels = ["@everyone"];

exports.help = "`list [stage number]` - lists all available classes or all classes at the given stage.";

exports.run = (client, message, args, config) => {
    let channels = message.guild.channels.array();
    channels = channels.filter(channel => channel.name.startsWith(config.domain + "-"));

    if (channels.length == 0) {
        message.channel.send("There are no available classes.");
        return;
    }

    channels.sort((a, b) => a.name.localeCompare(b.name));

    let responseMessage = "**Available " + (channels.length == 1 ? "class:" : "Classes") + "**\n";
    
    if (channels.length == 1) {
        responseMessage += channels.map(channel => channel.toString()).join("\n");
        message.channel.send(responseMessage);
        return;
    }

    let stage1 = channels.filter(channel => channel.name.startsWith(config.domain + "-" + "1"));
    let stage2 = channels.filter(channel => channel.name.startsWith(config.domain + "-" + "2"));
    let stage3 = channels.filter(channel => channel.name.startsWith(config.domain + "-" + "3"));
    stage1 = stage1.map(channel => channel.toString()).join("\n");
    stage2 = stage2.map(channel => channel.toString()).join("\n");
    stage3 = stage3.map(channel => channel.toString()).join("\n");

    if (args[0] == 1)
        responseMessage += "Stage 1:\n" + stage1;
    else if (args[0] == 2)
        responseMessage += "Stage 2:\n" + stage2;
    else if (args[0] == 3)
        responseMessage += "Stage 3:\n" + stage3;
    else
        responseMessage += "Stage 1:\n" + stage1 + "\nStage 2:\n" + stage2 + "\nStage 3:\n" + stage3;

    message.reply("you've been sent a direct message.");
    message.author.send(responseMessage);
}

// leave.js: Contains module for command 'leave'.
// Allows a user to unsubscribe from the given class(es).

exports.accessLevels = ["@everyone"];

exports.help = "`leave <class> [class2] [classN]` - leaves the given class(es).";

exports.run = async (client, message, args, config) => {
    if (args.length < 1) {
        message.channel.send("Usage: " + exports.help);
        return;
    }

    args = [...(new Set(args))]; // Remove duplicates.
    let member = message.member;
    let response = "";

    for (let i = 0; i < args.length; i++) {  
        let role = message.guild.roles.find("name", args[i]);
        
        if (!role || !role.name.startsWith(config.domain + "-")) {
            response += "- the class '" + args[i] + "' doesn't appear to exist. See `show`.\n";
            continue;
        }

        let channel = message.guild.channels.find("name", args[i]);
        let channelName = channel ? channel.toString() : ("`" + role.name + "`");

        if (!member.roles.has(role.id)) {
            response += "- you're not subscribed to " + channelName + ". See `show`.\n";
            continue;
        }
    
        await member.removeRole(role)
        .then(function() {
            response += "- you're now unsubscribed from " + channelName + ".\n";
        })
        .catch(function() {
            response += "- couldn't unsubscribe you from " + channelName + ".\n";
        });
    }
    
    message.reply("\n" + response);
}

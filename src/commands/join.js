// join.js: Contains module for command 'join'.
// Allows a user to subscribe to the given class(es).

exports.accessLevels = ["@everyone"];

exports.help = "`join <class> [class2] [classN]` - joins the given class(es).";

exports.run = async (client, message, args, config) => {
    if (args.length < 1) {
        message.channel.send("Usage: " + exports.help);
        return;
    }

    args = [...(new Set(args))]; // Remove duplicates.
    let member = message.member;
    let response = "";

    for (let i = 0; i < args.length; i++) {
        let role = message.guild.roles.find("name", args[i]);

        if (!role || !role.name.startsWith(config.domain + "-")) {
            response += "- the class '" + args[i] + "' doesn't appear to exist. See `list`.\n";
            continue;
        }
    
        let channel = message.guild.channels.find("name", args[i]);
        let channelName = channel ? channel.toString() : ("`" + role.name + "`");

        if (member.roles.has(role.id)) {
            response += "- you're already subscribed to " + channelName + ". See `show`.\n";
            continue;
        }

        await member.addRole(role)
        .then(function() {
            response += "- you're now subscribed to " + channelName + ".\n";
        })
        .catch(function() {
            response += "- couldn't subscribe you to " + channelName + ".\n";
        });
    }

    message.reply("\n" + response);
}

// help.js: Contains module for command 'help'.
// Sends the summoner a direct message with a list of available commands.

const fs = require("fs");

exports.accessLevels = ["@everyone"];

exports.help = "`help` - displays this help message.";

exports.run = (client, message, args, config) => {
    fs.readdir("./src/commands/", (err, files) => {
        let botCommandsChannel = message.guild.channels.find("name", "bot-commands");

        let helpLine = 
            "__**Class Manager Help**__"
            + "\nTo execute a command, you must prefix commands with either " + client.user + ", or `" + config.prefix + "`."
            + "\nCommands must only be issued in the " + botCommandsChannel + " channel."
            + "\n`<>` arguments are required, while `[]` arguments are optional."
            + "\nFor example, `+list 2` would list all stage 2 classes.\n\n"

        if (files.length == 0) {
            message.channel.send("**There are no available commands.**");
            return;
        }

        helpLine += "**Available " + (files.length == 1 ? "command" : "commands") + ":**\n";

        files.forEach(file => {
            try {
                let command = require("./" + file);
                let userRoleNames = message.member.roles.map(role => role.name);

                // If user has a role that is defined as allowed in command file:
                if (userRoleNames.some(role => command.accessLevels.includes(role)))
                    helpLine += command.help + "\n";
            }
            catch (err) {
                console.error(err);
            }
        });

        helpLine += "\nYou can check out the bot's `GitLab Repo` here: <https://gitlab.com/NoooneyDude/COMPSCIDiscordBot>.";

        message.reply("you've been sent a direct message.");
        message.author.send(helpLine);
    });
}

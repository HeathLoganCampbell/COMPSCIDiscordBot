const fs = require('fs');

let invitesHandler = {};
invitesHandler.invites = {};

invitesHandler.saveData = function() {
    try {
        fs.writeFileSync('./storage/invites.json', JSON.stringify(invitesHandler.invites, null, 2), 'utf8');
    } catch (err) {
        console.error(err);
    }    
}

invitesHandler.loadData = function() {
    try {
        invitesHandler.invites = JSON.parse(fs.readFileSync('./storage/invites.json', 'utf8'));
    } catch (err){
        console.error(err);
    }
}

invitesHandler.ensureData = function() {
    if (invitesHandler.invites === undefined || invitesHandler.invites === null || invitesHandler.invites == {} || Object.keys(invitesHandler.invites).length === 0) {
        invitesHandler.loadData();
    }
}

invitesHandler.getRolesFor = function(inviteID) {
    invitesHandler.ensureData();
    if (invitesHandler.invites.hasOwnProperty(inviteID)) {
        return invitesHandler[inviteID];
    } else {
        return false;
    }
}

invitesHandler.addRoleFor = function(inviteID, roleID) {
    invitesHandler.ensureData();
    if (!invitesHandler.invites.hasOwnProperty(inviteID)) {
        invitesHandler.invites[inviteID] = {
            "usages": 0,
            "roles": []
        };
    }
    invitesHandler.invites[inviteID]["roles"].push(roleID);
    invitesHandler.saveData();
}

invitesHandler.removeRoleFrom = function(inviteID, roleID) {
    invitesHandler.ensureData();
    if (!invitesHandler.invites.hasOwnProperty(inviteID)) {
        invitesHandler.invites[inviteID] = {
            "usages": 0,
            "roles": []
        };
        return;
    }
    var pos = invitesHandler.invites[inviteID]["roles"].indexOf(roleID);
    if (pos > -1) {
        invitesHandler.invites[inviteID]["roles"].splice(pos, 1);
    }
    invitesHandler.saveData();
}

invitesHandler.getInvites = function() {
    invitesHandler.ensureData();
    return invitesHandler.invites;
}

invitesHandler.markUsage = function(inviteID) {
    invitesHandler.ensureData();
    if(invitesHandler.invites.hasOwnProperty(inviteID)) {
        invitesHandler.invites[inviteID]["usages"] += 1;
    }
    invitesHandler.saveData();
}

invitesHandler.remove = function(inviteID) {
    invitesHandler.ensureData();
    delete invitesHandler.invites[inviteID];
    invitesHandler.saveData();
}

invitesHandler.setUsages = function(inviteID, useCount) {
    invitesHandler.ensureData();
    if(invitesHandler.invites.hasOwnProperty(inviteID)) {
        invitesHandler.invites[inviteID]["usages"] = useCount;
    }
    invitesHandler.saveData();
}

module.exports = invitesHandler;
